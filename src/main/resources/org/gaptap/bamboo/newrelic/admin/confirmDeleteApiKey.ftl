
<html>
<head>
	<meta name="decorator" content="adminpage">
</head>
<body>
	[@ui.header pageKey="newrelic.admin.sharedApiKey.heading" /]
	[@ui.clear /]
	[@ww.form action='/admin/newrelic/deleteSharedApiKey.action'
			submitLabelKey='newrelic.admin.sharedApiKey.button.delete'
			cancelUri='/admin/newrelic/configuration.action'
			showActionErrors='true']

		[@ww.hidden name='mode' /]
		[@ww.hidden name='apiKeyId' /]

		[@ui.messageBox type="warning" titleKey="newrelic.admin.sharedApiKey.delete.confirm.title"]
		    [@ww.text name="newrelic.admin.sharedApiKey.delete.confirm.message"]
		    [/@ww.text]
		[/@ui.messageBox]
	[/@ww.form]
</body>
</html>