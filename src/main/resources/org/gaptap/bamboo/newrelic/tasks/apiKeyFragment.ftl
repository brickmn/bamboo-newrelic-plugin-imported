[@ui.bambooSection titleKey='newrelic.config.api.section.title']

	[@ww.select labelKey='newrelic.config.deployment.apiKey.options' name='apiKeyOption'
	                                        listKey='key' listValue='value' toggle='true'
	                                        list=apiKeyOptions /]
	
	[@ui.bambooSection dependsOn="apiKeyOption" showOn="0"]
		[#if sharedApiKeys.size() == 0]
			[@ui.messageBox type="warning" titleKey="newrelic.config.deployment.apiKey.options.none.configured.title"]
			    [@ww.text name="newrelic.config.deployment.apiKey.options.none.configured.message"]
			    [/@ww.text]&nbsp;
			    [#if fn.hasAdminPermission()]
				    [@ww.text name="newrelic.config.deployment.apiKey.options.none.configured.add"]
				        [@ww.param]${req.contextPath}/admin/newrelic/configuration.action[/@ww.param]
				    [/@ww.text]
			    [/#if]
			[/@ui.messageBox]
		[/#if]
	    [@ww.select labelKey='newrelic.config.deployment.apiKey.options.shared' name='sharedApiKeyId'
	                                        listKey='key' listValue='value' toggle='true'
	                                        list=sharedApiKeys /]
	[/@ui.bambooSection]
	
	[@ui.bambooSection dependsOn="apiKeyOption" showOn="1"]
		[@ww.textfield labelKey='newrelic.config.deployment.apiKey' name='apiKey' required='true' cssClass="long-field" /]
	[/@ui.bambooSection]

[/@ui.bambooSection]