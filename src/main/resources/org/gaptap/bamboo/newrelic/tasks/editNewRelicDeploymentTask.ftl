[#include "apiKeyFragment.ftl"]

[@ui.bambooSection titleKey='newrelic.config.deployment.section.title']
	[@ww.textfield labelKey='newrelic.config.deployment.appName' name='appName' required='true' /]
[/@ui.bambooSection]

[#include "advancedFragment.ftl"]