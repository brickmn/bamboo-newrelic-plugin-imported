
newrelic.config.required.field=Required Field.
newrelic.config.validation.integer=Must be an integer.

# Shared Task Configuration

newrelic.config.api.section.title=API Key
newrelic.config.deployment.apiKey.options=API Key Options
newrelic.config.deployment.apiKey.options.shared=Shared API Key
newrelic.config.deployment.apiKey.options.local=Specify Below
newrelic.config.deployment.apiKey.options.none.configured.title=No Shared API Keys Are Configured
newrelic.config.deployment.apiKey.options.none.configured.message=Shared API keys can be configured by administrators.
newrelic.config.deployment.apiKey.options.none.configured.add=<a href="{0}">Configure a new shared API key.</a>

newrelic.config.deployment.apiKey=Deployment API Key
newrelic.config.deployment.apiKey.description=Your Deployment API Key. This may differ from your primary account API key. Find this key by going to <em>Events > Deployment</em> within your New Relic application dashboard.

newrelic.config.deployment.advanced=Advanced

newrelic.config.deployment.customProxy=Use proxy server
newrelic.config.deployment.customProxy.description=Use a custom proxy server
newrelic.config.deployment.proxy.options=Proxy Server Options
newrelic.config.deployment.proxy.options.local=Specify Below
newrelic.config.deployment.proxy.options.shared=Shared Proxy Server
newrelic.config.deployment.proxy.options.none.configured.title=No Shared Proxy Servers Are Configured
newrelic.config.deployment.proxy.options.none.configured.message=Shared proxy servers can be configured by administrators
newrelic.config.deployment.proxy.options.none.configured.add=<a href="{0}">Configure a new shared proxy server.</a>

newrelic.config.deployment.proxy.host=Host
newrelic.config.deployment.proxy.port=Port

newrelic.config.deployment.proxy.options.shared=Shared Proxy Server

# Deployment Task Configuration

newrelic.config.deployment.section.title=Deployment Information

newrelic.config.deployment.appName=Application Name
newrelic.config.deployment.appName.description=The name of the application, found in the newrelic.yml file.

# Application Task Configuration

newrelic.config.application.section.title=Application Information

newrelic.config.application.applicationId=Application ID
newrelic.config.application.applicationId.description=The ID of the application as found in the New Relic UI.

newrelic.config.application.data.options=Data Location
newrelic.config.application.data.option.file=File
newrelic.config.application.data.option.inline=Inline

newrelic.config.application.requestFile=Request Data File
newrelic.config.application.requestFile.description=Path to the JSON file, relative to the working directory. The file content will be posted to the New Relic v2 Application API. See <a href="https://rpm.newrelic.com/api/explore/applications/update" target="_blank">https://rpm.newrelic.com/api/explore/applications/update</a>.
newrelic.config.application.inlineData=Request Data
newrelic.config.application.inlineData.description=JSON to be posted to the New Relic v2 Application API. See <a href="https://rpm.newrelic.com/api/explore/applications/update" target="_blank">https://rpm.newrelic.com/api/explore/applications/update</a>.

# Administration

websections.system.admin.newrelic=New Relic
webitems.system.admin.newrelic.configuration=Shared Configuration

newrelic.admin.validation.errors=The data submitted is invalid.

newrelic.admin.overview.description=Integrate Bamboo with New Relics' deployment tracking. Quickly see how your application deployments effect your application's performance.

newrelic.admin.sharedConfig.title=New Relic Shared Configuration
newrelic.admin.sharedConfig.heading=New Relic Shared Configuration

newrelic.admin.sharedConfig.tab.heading=Config
newrelic.admin.sharedApiKey.tab.heading=API Keys
newrelic.admin.sharedProxy.tab.heading=Proxy Servers

newrelic.admin.messages.action=Successfully {0}.
newrelic.admin.messages.action.added.apiKey=added API Key
newrelic.admin.messages.action.updated.apiKey=updated API Key
newrelic.admin.messages.action.deleted.apiKey=deleted API Key
newrelic.admin.messages.action.added.proxy=added proxy server
newrelic.admin.messages.action.updated.proxy=updated proxy server
newrelic.admin.messages.action.deleted.proxy=deleted proxy server

# Admin: API Keys

newrelic.admin.sharedApiKey.description=Configure API Keys that can be shared between tasks.

newrelic.admin.sharedApiKey.list.heading=Available API Keys
newrelic.admin.sharedApiKey.list.apiKey=API Key
newrelic.admin.list.heading.sharedApiKey=Name
newrelic.admin.list.heading.configuration=Configuration
newrelic.admin.list.heading.operations=Operations

newrelic.admin.sharedApiKey.add=Add API Key
newrelic.admin.sharedApiKey.edit=Edit
newrelic.admin.sharedApiKey.delete=Delete

newrelic.admin.sharedApiKey.none=No API keys are configured.
newrelic.admin.sharedApiKey.none.help=Configure new Shared API keys by clicking the Add API Key button.

newrelic.admin.sharedApiKey.heading=New Relic Shared API Key
newrelic.admin.sharedApiKey.form.title=API Key
newrelic.admin.sharedApiKey.form.overview=Please provide API key details.
newrelic.admin.sharedApiKey.form.name=Name
newrelic.admin.sharedApiKey.form.name.description=A name for the shared API key.
newrelic.admin.sharedApiKey.form.apiKey=Deployment API Key
newrelic.admin.sharedApiKey.form.apiKey.description=Your Deployment API Key. This may differ from your primary account API key. Find this key by going to <em>Events > Deployment</em> within your New Relic application dashboard.
newrelic.admin.sharedApiKey.form.description=Description
newrelic.admin.sharedApiKey.form.description.description=A short description.

newrelic.admin.sharedApiKey.button.update=Update
newrelic.admin.sharedApiKey.button.create=Save
newrelic.admin.sharedApiKey.button.delete=Delete

newrelic.admin.sharedApiKey.delete.confirm.title=Confirm Delete
newrelic.admin.sharedApiKey.delete.confirm.message=Are you sure you want to delete the API Key? Please verify that that no Tasks are using this key before proceeding.

# Admin: Proxies

newrelic.admin.sharedProxy.description=Configure proxy servers that can be shared between tasks.

newrelic.admin.sharedProxy.list.heading=Available Proxy Servers
newrelic.admin.sharedProxy.list.host=Host
newrelic.admin.sharedProxy.list.port=Port
newrelic.admin.list.heading.sharedProxy=Name
newrelic.admin.list.heading.configuration=Configuration
newrelic.admin.list.heading.operations=Operations

newrelic.admin.sharedProxy.add=Add Proxy Server
newrelic.admin.sharedProxy.edit=Edit
newrelic.admin.sharedProxy.delete=Delete

newrelic.admin.sharedProxy.none=No proxy servers are configured.
newrelic.admin.sharedProxy.none.help=Configure new shared proxy server by clicking the Add Proxy Server button.

newrelic.admin.sharedProxy.heading=New Relic Shared Proxy Server
newrelic.admin.sharedProxy.form.title=Proxy Server
newrelic.admin.sharedProxy.form.overview=Please provide proxy server details.
newrelic.admin.sharedProxy.form.name=Name
newrelic.admin.sharedProxy.form.name.description=A name for the shared proxy server.
newrelic.admin.sharedProxy.form.host=Host
newrelic.admin.sharedProxy.form.port=Port
newrelic.admin.sharedProxy.form.description=Description
newrelic.admin.sharedProxy.form.description.description=A short description.

newrelic.admin.sharedProxy.button.update=Update
newrelic.admin.sharedProxy.button.create=Save
newrelic.admin.sharedProxy.button.delete=Delete

newrelic.admin.sharedProxy.delete.confirm.title=Confirm Delete
newrelic.admin.sharedProxy.delete.confirm.message=Are you sure you want to delete the proxy server? Please verify that that no Tasks are using this proxy server before proceeding.
