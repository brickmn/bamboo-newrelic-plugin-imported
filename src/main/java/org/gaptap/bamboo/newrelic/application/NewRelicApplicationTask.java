/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.application;

import static org.gaptap.bamboo.newrelic.application.NewRelicApplicationTaskConfigurator.*;

import java.io.File;

import org.gaptap.bamboo.newrelic.AbstractNewRelicTask;
import org.gaptap.bamboo.newrelic.ConnectionParams;
import org.gaptap.bamboo.newrelic.NewRelicApiException;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;

/**
 * @author David Ehringer
 */
public class NewRelicApplicationTask extends AbstractNewRelicTask {

    @Override
    public TaskResult execute(@NotNull CommonTaskContext commonTaskContext) throws TaskException {
        BuildLogger buildLogger = commonTaskContext.getBuildLogger();
        buildLogger.addBuildLogEntry("Updating application configuration in New Relic");

        String apiKey = getApiKey(commonTaskContext);
        String applicationId = commonTaskContext.getConfigurationMap().get(APP_ID);

        ApplicationInfo applicationInfo = null;

        String dataOption = commonTaskContext.getConfigurationMap().get(SELECTED_DATA_OPTION);
        if (DATA_OPTION_FILE.equals(dataOption)) {
            String requestContentsFileName = commonTaskContext.getConfigurationMap().get(REQUEST_FILE);
            File requestContents = new File(commonTaskContext.getRootDirectory(), requestContentsFileName);
            applicationInfo = new ApplicationInfo(apiKey, applicationId, requestContents);
        } else if (DATA_OPTION_INLINE.equals(dataOption)) {
            String data = commonTaskContext.getConfigurationMap().get(INLINE_DATA);
            applicationInfo = new ApplicationInfo(apiKey, applicationId, data);
        } else {
            String message = "Invalid value for " + SELECTED_DATA_OPTION;
            buildLogger.addErrorLogEntry(message);
            throw new IllegalArgumentException(message);
        }

        ApplicationUpdater updater = new DefaultApplicationUpdater();
        try {
            buildLogger.addBuildLogEntry("Sending " + applicationInfo);
            if (useProxy(commonTaskContext)) {
                ConnectionParams connectionParams = getConnectionParams(commonTaskContext);
                buildLogger.addBuildLogEntry("Using proxy " + connectionParams);
                updater.update(applicationInfo, connectionParams, buildLogger);
            } else {
                updater.update(applicationInfo, buildLogger);
            }
        } catch (NewRelicApiException e) {
            return TaskResultBuilder.newBuilder(commonTaskContext).failedWithError().build();
        }

        return TaskResultBuilder.newBuilder(commonTaskContext).success().build();
    }

}
