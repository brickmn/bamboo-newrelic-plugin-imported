/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.newrelic.deployment.admin.NewRelicAdminService;
import org.gaptap.bamboo.newrelic.deployment.admin.SharedApiKey;
import org.gaptap.bamboo.newrelic.deployment.admin.SharedProxyServer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 */
public class AbstractNewRelicTaskConfigurator extends AbstractTaskConfigurator {

    public static final String API_KEY_OPTION = "apiKeyOption";
    public static final String API_KEY_OPTION_SHARED = "0";
    public static final String API_KEY_OPTION_LOCAL = "1";
    public static final String API_KEY = "apiKey";
    public static final String SHARED_API_KEY_ID = "sharedApiKeyId";

    public static final String CUSTOM_PROXY = "customProxy";
    public static final String CUSTOM_PROXY_ENABLED = "true";
    public static final String PROXY_OPTION = "proxyOption";
    public static final String PROXY_OPTION_SHARED = "0";
    public static final String PROXY_OPTION_LOCAL = "1";
    public static final String PROXY_HOST = "proxyHost";
    public static final String PROXY_PORT = "proxyPort";
    public static final String SHARED_PROXY_ID = "sharedProxyId";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(API_KEY_OPTION, API_KEY, SHARED_API_KEY_ID,
            CUSTOM_PROXY, PROXY_OPTION, PROXY_HOST, PROXY_PORT, SHARED_PROXY_ID);

    protected final TextProvider textProvider;
    private final NewRelicAdminService newRelicAdminService;

    public AbstractNewRelicTaskConfigurator(NewRelicAdminService newRelicAdminService, TextProvider textProvider,
            TaskConfiguratorHelper taskConfiguratorHelper) {
        this.newRelicAdminService = newRelicAdminService;
        this.textProvider = textProvider;
        this.taskConfiguratorHelper = taskConfiguratorHelper;
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params,
            @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
        context.put(API_KEY_OPTION, API_KEY_OPTION_LOCAL);
        context.put(CUSTOM_PROXY, "false");
        context.put(PROXY_OPTION, PROXY_OPTION_LOCAL);
        populateContextForAll(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context,
            @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
        populateContextForAll(context);
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context,
            @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
        populateContextForAll(context);
    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> apiKeyOptions = Maps.newHashMap();
        apiKeyOptions
                .put(API_KEY_OPTION_LOCAL, textProvider.getText("newrelic.config.deployment.apiKey.options.local"));
        apiKeyOptions.put(API_KEY_OPTION_SHARED,
                textProvider.getText("newrelic.config.deployment.apiKey.options.shared"));
        context.put("apiKeyOptions", apiKeyOptions);

        Map<String, String> sharedKeys = Maps.newHashMap();
        for (SharedApiKey sharedApiKey : newRelicAdminService.allSharedApiKeys()) {
            sharedKeys.put(String.valueOf(sharedApiKey.getID()), sharedApiKey.getName());
        }
        context.put("sharedApiKeys", sharedKeys);

        Map<String, String> proxyOptions = Maps.newHashMap();
        proxyOptions.put(PROXY_OPTION_LOCAL, textProvider.getText("newrelic.config.deployment.proxy.options.local"));
        proxyOptions.put(PROXY_OPTION_SHARED, textProvider.getText("newrelic.config.deployment.proxy.options.shared"));
        context.put("proxyOptions", proxyOptions);

        Map<String, String> sharedProxies = Maps.newHashMap();
        for (SharedProxyServer sharedProxy : newRelicAdminService.allSharedProxyServers()) {
            sharedProxies.put(String.valueOf(sharedProxy.getID()), sharedProxy.getName());
        }
        context.put("sharedProxies", sharedProxies);
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        if (API_KEY_OPTION_LOCAL.equals(params.getString(API_KEY_OPTION))) {
            if (StringUtils.isEmpty(params.getString(API_KEY))) {
                errorCollection.addError(API_KEY, textProvider.getText("newrelic.config.required.field"));
            }
        } else {
            if (StringUtils.isEmpty(params.getString(SHARED_API_KEY_ID))) {
                errorCollection.addError(SHARED_API_KEY_ID, textProvider.getText("newrelic.config.required.field"));
            }
        }

        if (CUSTOM_PROXY_ENABLED.equals(params.getString(CUSTOM_PROXY))) {
            if (PROXY_OPTION_LOCAL.equals(params.getString(PROXY_OPTION))) {
                if (StringUtils.isEmpty(params.getString(PROXY_HOST))) {
                    errorCollection.addError(PROXY_HOST, textProvider.getText("newrelic.config.required.field"));
                }
                String port = params.getString(PROXY_PORT);
                if (StringUtils.isEmpty(port)) {
                    errorCollection.addError(PROXY_PORT, textProvider.getText("newrelic.config.validation.integer"));
                } else {
                    try {
                        Integer.parseInt(port);
                    } catch (NumberFormatException e) {
                        errorCollection.addError(PROXY_PORT, textProvider.getText("newrelic.config.required.field"));
                    }
                }
            } else {
                if (StringUtils.isEmpty(params.getString(SHARED_PROXY_ID))) {
                    errorCollection.addError(SHARED_PROXY_ID, textProvider.getText("newrelic.config.required.field"));
                }
            }
        }
    }
}