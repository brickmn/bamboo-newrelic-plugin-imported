/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.deployment.admin;

import java.util.List;

/**
 * @author David Ehringer
 */
public interface NewRelicAdminService {

    SharedApiKey addSharedApiKey(String name, String apiKey, String description);

    SharedApiKey updateSharedApiKey(int id, String name, String apiKey, String description);

    SharedApiKey getSharedApiKey(int id);

    List<SharedApiKey> allSharedApiKeys();

    void deleteSharedApiKey(int id);

    SharedProxyServer addSharedProxyServer(String name, String host, int port, String description);

    SharedProxyServer updateSharedProxyServer(int id, String name, String host, int port, String description);

    SharedProxyServer getSharedProxyServer(int id);

    List<SharedProxyServer> allSharedProxyServers();

    void deleteSharedProxyServer(int id);
}
