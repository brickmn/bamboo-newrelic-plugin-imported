/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.deployment;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.newrelic.AbstractNewRelicTaskConfigurator;
import org.gaptap.bamboo.newrelic.deployment.admin.NewRelicAdminService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableList;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 */
public class NewRelicDeploymentTaskConfigurator extends AbstractNewRelicTaskConfigurator {

    public static final String APP_NAME = "appName";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(APP_NAME);

    public NewRelicDeploymentTaskConfigurator(NewRelicAdminService newRelicAdminService, TextProvider textProvider,
            TaskConfiguratorHelper taskConfiguratorHelper) {
        super(newRelicAdminService, textProvider, taskConfiguratorHelper);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params,
            @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context,
            @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        if (StringUtils.isEmpty(params.getString(APP_NAME))) {
            errorCollection.addError(APP_NAME, textProvider.getText("newrelic.config.required.field"));
        }
    }
}
